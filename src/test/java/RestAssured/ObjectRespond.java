package RestAssured;

import com.google.gson.annotations.SerializedName;
import lombok.Data;


@Data
public class ObjectRespond {
    @SerializedName("name")
    public String name;

    @SerializedName("job")
    public String job;

    @SerializedName("rank")
    public String rank;

    @SerializedName("id")
    public String id;

    @SerializedName("createdAt")
    public String createdAt;
}
