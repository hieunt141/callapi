package SerializedName;

import RestAssured.ObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SerializedNameExample {

    public static void main(String[] args) {
        final GsonBuilder builder = new GsonBuilder();
        final Gson gson = builder.create();

//        final ObjectRequest objectRequest = new ObjectRequest("Hieu","IT","Gold");
//        System.out.println(objectRequest);

        ObjectRequest objectRequest = new ObjectRequest();
        String json2="{\"name em\":\"Hieu\",\"Job em\":\"IT\",\"Rank em\":\"Gold\"}";

        final String json = gson.toJson(objectRequest);
        System.out.println("Json: " + json);

        final ObjectRequest student2 = gson.fromJson(json2, ObjectRequest.class);//giải mã 1 String json đc chỉ định thành 1 obj của 1 class đc chỉ định
        System.out.println("Java Object: " + student2);
    }

}
